public class Fila {
    private int[] numeros;
    private int inicio;
    private int fim;
    private int total;

    public Fila(int tamanho){
        numeros = new int[tamanho];
        inicio = 0;
        fim = 0;
        total = 0;
    }
    public void inserir(int numero)throws Exception{
        if (isFull()){
            throw new Exception("Fila está cheia");
        }else{
            numeros[fim] = numero;
            fim = (fim + 1) % numeros.length;
            total++;
        }

    }
    public int retirar() throws Exception{
        if (isEmpty()){
            throw new Exception("Fila está vazia");
        }else{
            int c = numeros[inicio];
            inicio = (inicio + 1) % numeros.length;
            total--;
            return c;
        }
    }
    public boolean isEmpty(){
        return total == 0;
    }
    public boolean isFull(){
        return total == numeros.length;
    }

}
