import java.util.Scanner;

public class Questao01 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        Pilha pa = new Pilha(5);
        try {
            pa.empilhar(2);
            pa.empilhar(3);
            pa.empilhar(4);
            pa.empilhar(5);
            pa.empilhar(6);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        Pilha pb = new Pilha(5);
        try {
            pb.empilhar(2);
            pb.empilhar(3);
            pb.empilhar(4);
            pb.empilhar(5);
            pb.empilhar(6);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("As pilhas são iguais: " + Iguais(pa, pb));
    }
    public static boolean Iguais(Pilha pilhaA, Pilha pilhaB){
        int cont = 0;
        for (int i = 0; i < pilhaA.getObjetos().length; i++){
            try {
                if (pilhaA.desempilhar() == pilhaB.desempilhar() && pilhaA.getObjetos().length == pilhaB.getObjetos().length){
                    cont++;
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
        pilhaA.setTopodaPilha(pilhaA.getObjetos().length -1);
        pilhaB.setTopodaPilha(pilhaB.getObjetos().length - 1);
        if (cont == pilhaA.getObjetos().length){
            return true;
        }else{
            return false;
        }
    }

}
