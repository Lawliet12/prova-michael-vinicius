public class Tipo {
    final int MaiorQZeroPar;
    final int MaiorQZeroImpar;
    final int MenorIgualZeroPar;
    final int MenorIgualZeroImpar;

    public Tipo(int maiorQZeroPar, int maiorQZeroImpar, int menorIgualZeroPar, int menorIgualZeroImpar) {
        MaiorQZeroPar = maiorQZeroPar;
        MaiorQZeroImpar = maiorQZeroImpar;
        MenorIgualZeroPar = menorIgualZeroPar;
        MenorIgualZeroImpar = menorIgualZeroImpar;
    }
}
