public class Pilha implements IPilha{
    private Object[] objetos;
    private int topodaPilha;

    public Pilha(int tam){
        objetos = new Object[tam];
        topodaPilha = -1;
    }
    public void empilhar(Object o) throws Exception {
        if (estaCheia()){
            throw new Exception("Pilha está cheia");
        }else{
            topodaPilha++;
            objetos[topodaPilha] = o;
        }

    }

    public Object desempilhar() throws Exception {
        if (estaVazia()){
            throw new Exception("Pilha está vazia");
        }else{
            Object o = objetos[topodaPilha];
            topodaPilha--;
            return o;
        }
    }

    public boolean estaVazia() {
        if (topodaPilha == -1){
            return true;
        }else{
            return false;
        }
    }
    public boolean estaCheia(){
        if (topodaPilha == objetos.length - 1){
            return true;
        }else{
            return false;
        }
    }

    public Object[] getObjetos() {
        return objetos;
    }

    public void setTopodaPilha(int topodaPilha) {
        this.topodaPilha = topodaPilha;
    }
}
