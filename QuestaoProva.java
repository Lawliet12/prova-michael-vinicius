public abstract class QuestaoProva {
    public abstract void adicionar(int valor);
    public abstract int obterProximo(Tipo tipo);
    public abstract void excluirTodos(Tipo tipo);
    int capacidadeMaxPorTipo = 0;
    public QuestaoProva(int capacidadeMaxPorTipo){
        this.capacidadeMaxPorTipo = capacidadeMaxPorTipo;
    }
}
